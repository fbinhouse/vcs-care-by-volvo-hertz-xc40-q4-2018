* The current logic supports use of 3 versions of images - normal(=desktop), mobile-lowres, mobile-highres. Place images in correct folders under the img-folder.

* Go into the repo in terminal and type "npm install"

* fbinhouse-injector.js: Add or remove desired external scripts & css files.

* When running the project, run "grunt watch" to have grunt do build tasks along the way

* Give the script to Madeleine and she will insert it into our page
