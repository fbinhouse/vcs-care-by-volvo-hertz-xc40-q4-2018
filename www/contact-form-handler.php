<?php

header('Content-type: application/json');
header('Access-Control-Allow-Headers: Content-Type');
header("Access-Control-Allow-Origin: *");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

if ($_POST) {
    $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $firstname = filter_var($_POST['firstname']);
    $lastname = filter_var($_POST['lastname']);
    $ssn = filter_var($_POST['ssn']);
    $useremailaddress = filter_var($_POST['emailaddress']);
    $phonenumber = filter_var($_POST['phonenumber']);
    $street = filter_var($_POST['street']);
    $secondaddress = filter_var($_POST['secondaddress']);
    $zipcode = filter_var($_POST['zipcode']);
    $city = filter_var($_POST['city']);
    $color = filter_var($_POST['color']);
    $carCity = filter_var($_POST['carCity']);
    $startDate = filter_var($_POST['startDate']);
    $length = filter_var($_POST['length']);

    $hertzSubject = "CBV bilbokning";
    $userSubject = "Tack för din bilbokning";

    $hertzMessage = '<html>
    <body>
    <h1>CBV bilbokning</h1>
    <p>
        <h2>Kunduppgifter</h2>
        <strong>Förnamn: </strong>'. $firstname . '<br>
        <strong>Efternamn: </strong>'. $lastname . '<br>
        <strong>Personnummer: </strong>'. $ssn . '<br>
        <strong>Epostadress: </strong>'. $useremailaddress . '<br>
        <strong>Telefonnummer: </strong>'. $phonenumber . '<br>
        <strong>Gatuadress: </strong>'. $street . '<br>
        <strong>Adress2: </strong>'. $secondaddress . '<br>
        <strong>Postnummer: </strong>'. $zipcode . '<br>
        <strong>Ort: </strong>'. $city . '<br>
        <h2>Bilval</h2>
        <strong>Färg:</strong>'. $color . '<br>
        <strong>Ort: </strong>'. $carCity . '<br>
        <strong>Önskat upphämtningsdatum: </strong>'. $startDate . '<br>
        <strong>Önskad abonnemangsperiod: </strong>'. $length . ' månader<br>
    </p>
    <img src="http://static.fbinhouse.se/cbvlogomailheader.png" alt="Care by Volvo logo">
    </body>
    </html>';

    $hertzAltMessage = "CBV bilbokning:
    Kunduppgifter
    Förnamn: $firstname
    Efternamn: $lastname
    Personnummer: $ssn
    Epostadress: $useremailaddress
    Telefonnummer: $phonenumber
    Gatuadress: $street
    Adress2: $secondaddress
    Postnummer: $zipcode
    Ort: $city
    Bilval
    Färg: $color
    Ort: $carCity
    Önskat startdatum: $startDate
    Önskad abonnemangsperiod: $length månader";

    $userMessage = '
    <!DOCTYPE html "-//w3c//dtd html 4.01 transitional//en" "http://www.w3.org/tr/html4/loose.dtd"><html><head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">


        <style type="text/css">
            img,image{border:0;}a:link,a:active,a:visited{color:#003057;text-decoration:none;} body[yahoo] .phoneLink a {
                color: #003057 !important;
            }

            body[yahoo] {
                margin: 0;
                padding: 0;
                -webkit-text-size-adjust: 100%;
            }

                body[yahoo] table, td {
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                }

                body[yahoo] logo {
                    width: 50px;
                }

                body[yahoo] a.hoverlink:hover {
                    color: #003057 !important;
                    background-color: #ffffff !important;
                }

                    body[yahoo] a.hoverlink:hover span {
                        color: #003057 !important;
                        background-color: #ffffff !important;
                    }

                body[yahoo] div.changeDivColor:hover a span.hoverlink1 {
                    color: #ffffff !important;
                    background: #003057 !important;

                }

                body[yahoo] .changeDivColor:hover {
                    background-color: #003057 !important;
                }

                body[yahoo] .changeDivColor:hover a{
                    color: #ffffff !important;
                }


                /*body[yahoo] div td.hoverBTN:hover {
                    color: #ffffff !important;
                    background: #003057 !important;
                }*/

                body[yahoo] a.hoverlink1:hover {
                    color: #ffffff !important;
                    background-color: #003057 !important;
                }

                    body[yahoo] a.hoverlink1:hover span {
                        color: #ffffff !important;
                        background-color: #003057 !important;
                    }

    			.appleLinksWhite a {
    				color: #ffffff !important;
    				text-decoration: none !important;
    			}


            @media screen and (max-width: 480px), screen and (max-device-width: 480px) {
                /* mobile-specific CSS styles go here */

                body[yahoo] {
                    width: 100% !important;
                    min-width: 100% !important;
                }

                    body[yahoo] td.hoverBTN:hover {
                        color: #ffffff !important;
                        background: #003057 !important;
                        padding: 0px 0px !important;
                        border: 0px !important;
                    }


                    body[yahoo] td.hoverBTN {
                        display: table !IMPORTANT;
                        margin: auto !important;
                    }




                    body[yahoo] td.hoverBTN01:hover {
                        padding: 12px 18px !important;
                        border: 1px solid #003057 !important;
                    }

                    body[yahoo] table {
                        width: 100% !important;
                        line-height: 20px;
                    }

                    body[yahoo] .fullWidth {
                        width: 100% !important;
                    }

                    body[yahoo] .center-background {
                        background-position: center !important;
                        background-size: auto 100% !important;
                        background-repeat: no-repeat !important;
                    }

                    body[yahoo] tr.50Width {
                        width: 50%;
                    }


                    body[yahoo] .brodtextstorrubrik {
                        font-size: 28px !important;
                        line-height: 34px !important;
                    }

                    body[yahoo] .brodtextrubrik {
                        font-size: 24px !important;
                        line-height: 30px !important;
                    }

                    body[yahoo] .brodtextspec {
                        font-size: 20px !important;
                        line-height: 26px !important;
                    }

                    body[yahoo] .brodtext {
                        font-size: 14px !important;
                        line-height: 22px !important;
                    }

                    body[yahoo] .textpadding {
                        padding: 0 0 30px 0 !important;
                    }

                    body[yahoo] .brodtext2 {
                        font-size: 14px !important;
                        line-height: 32px !important;
                    }

                    body[yahoo] .imagepadding {
                        padding: 10px 0 45px 0 !important;
                    }

                    body[yahoo] .padding-sides-20 {
                        padding-left: 20px !important;
                        padding-right: 20px !important;
                    }

                    body[yahoo] .padding-sides-0 {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }

                    body[yahoo] .padding-top-30 {
                        padding-top: 30px !important;
                    }

                    body[yahoo] .marginal2 {
                        padding-left: 10px !important;
                        padding-right: 10px !important;
                    }

                    body[yahoo] .marginal3 {
                        padding-left: 20px !important;
                        padding-right: 20px !important;
                        padding-bottom: 55px !important;
                    }

                    body[yahoo] .logo {
                        width: 45px !important;
                        height: 45px !important;
                    }

                    body[yahoo] .ejmob {
                        display: none !important;
                        width: 0px !important;
                        overflow: hidden !important;
                    }

                    body[yahoo] .mobilebreak {
                        display: block;
                    }

                    body[yahoo] img {
                        display: block;
                        max-width: 100%;
                        margin: 0 auto;
                    }

                    body[yahoo] .imagepx {
                        width: 100% !important;
                    }

                    body[yahoo] .ejcen {
                        width: 48px !important;
                    }

                    body[yahoo] .ejcen3 {
                        width: 5px !important;
                    }

                    body[yahoo] .ejcen2 {
                        width: 48px !important;
                        padding-right: 0 !important;
                    }

                    body[yahoo] .topp {
                        padding-top: 0 !important;
                    }

                    body[yahoo] .linkunderline {
                        text-decoration: underline !important;
                    }

                    body[yahoo] a.hoverlink:active {
                        color: #ffffff !important;
                        background-color: #003057 !important;
                    }

                    body[yahoo] .tillbehor {
                        display: inline !important;
                        float: none !important;
                        width: 50% !important;
                    }

                    body[yahoo] .featuremob {
                        height: 15px !important;
                    }

                    body[yahoo] .phoneLink {
                        text-decoration: underline !important;
                    }
            }
        </style>

        <title>Nyhetsbrev fr&#229;n Volvo Car Sverige</title>



















    </head>


    <body style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%" yahoo="fix" bgcolor="#F6F6F6">

        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100% !important" border="0" cellspacing="0" cellpadding="0" bgcolor="#F6F6F6" width="100%">
            <tbody><tr>
                <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" align="center" valign="top" id="skal">

                    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="640" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
                        <tbody><tr>
                            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" align="left">

                                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody><tr>
                                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" valign="top">

                                        </td>
                                    </tr>
                                </tbody></table>

                                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody><tr>
                                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" valign="top">


    <table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
        <tbody><tr>
            <td class="logoTop" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 5px 0 5px 0" width="640" align="center"><img class="imagesize" style="border: none;display: block;width: 50px;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/10.png" width="50" height="50" border="0">
            </td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%"><img class="fullWidth" style="border: 0;display: block;width: 640px" src="https://static.fbinhouse.se/Hero_640x320_x2.jpg" alt="CBV Header" width="640" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td class="padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 50px 0 50px">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td class="brodtext" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: center;padding: 0 0 10px 0" valign="top" align="center"><b>CARE BY VOLVO</b></td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td class="brodtextstorrubrik padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 26px;line-height: 32px;font-family: Helvetica, Arial, sans-serif;color: #003057;padding: 0 50px 5px 50px;text-align: center" valign="top" align="center"><strong>Välkommen till Care by Volvo</strong></td>
                    </tr>
                    <tr>
                        <td class="brodtext padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: center;padding: 0 50px 0 50px" valign="top" align="center">Du har precis startat ett abonnemang hos Care by Volvo i samarbete med Hertz. För att du ska känna dig trygg har vi sammanfattat allt som ingår och vad du behöver veta här nedanför.</td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 20px;line-height: 0px;font-size: 20px;mso-line-height-rule: exactly" height="20px"><img style="border: 0;display: block;height: 20px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="20" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>





    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td class="brodtextstorrubrik padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 26px;line-height: 32px;font-family: Helvetica, Arial, sans-serif;color: #003057;padding: 0 20px 5px 60px;text-align: left" valign="top" align="center"><strong>I ditt abonnemang ingår</strong></td>
                    </tr>

                </tbody></table>
            </td>
        </tr>
    </tbody></table>


    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="font-family: Helvetica, Arial, sans-serif; mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 14px; padding: 0 20px 0 20px; line-height: 18px; mso-line-height-rule: exactly" >
                <table border="0" cellspacing="0" cellpadding="0" style="margin: auto; color:#003057;">
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">En nästan ny Volvo V40 för 4450 kr/månad</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">Försäkringar, skatter, service, vinterdäck och förvaring</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">Volvo on Call</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">Klimatpaket med eluppvärmt baksäte och bränslevärmare</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">In-Car Delivery</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">Sunfleet-medlemskap med 10 värdecheckar à 500 kr/st</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">Upp till 20 % hyrbilsrabatt på Hertz i Sverige. Ange CDP 852 099</td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px 4px">&bull;</td>
                        <td style="font-size: 14px; padding: 5px 0" valign="top">1500 mil/år eller 125 mil/månad (eventuella övermil debiteras med 6 kr inkl. moms)</td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody></table>

    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>

    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td class="padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 50px 0 50px" width="100%"><img class="fullWidth" style="border: 0;display: block;width: 540px" src="https://static.fbinhouse.se/bild_sektion2_540x230_x2.jpg" alt="Intervju" ignore="true" width="540"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td class="padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 50px 0 50px" valign="top">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt">
                            <table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border: none;" border="0" cellspacing="0" cellpadding="0" align="left">
                                <tbody><tr>
                                    <td class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;">
                                        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td class="brodtextstorrubrik topp" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 26px;line-height: 32px;font-family: Helvetica, Arial, sans-serif;color: #003057;padding: 15px 0 10px 0;text-align: left" valign="top" align="center"><strong>Regler & villkor</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="brodtext textpadding" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: left;padding: 00" valign="top" align="center">Uppsägningstiden är 30 dagar och kan maximalt bokas i 12 månader med eventuell förlängning på ytterligare 6 månader. Erbjudandet gäller för uthämtning på utvalda och av Hertz anvisade uthyrningskontor (Stockholm, Göteborg och Malmö) med reservation för slutförsäljning. Eventuella tillval som barnstolar, takräcken eller liknande betalas vid uthyrningskontoret med kredit/betal/bankkort.<br><br>Betalning för bilen, eventuella trängselskatter, parkeringsböter, broavgifter eller liknande faktureras i efterskott månatligen med 30 dagars betalningsvillkor. I övrigt gäller de villkor som finns att läsa här: <a href="https://assets.volvocars.com/se/~/media/sweden/downloads/CbV_Hertz_Allmanna_villkor_hyresavtal_20181009.pdf">Läs villkoren (PDF)</a><br /><br />
                                                Avisering om uppsägning av tjänsten kan ske skriftligen till carebyvolvo@hertz.se eller direkt till uthyrningskontoret där du hämtade ut din V40. Varje ny påbörjad månadsperiod debiteras med en full månadskostnad. Uppsägning av abonnemanget kan ske fram till datum för ny påbörjad månadsperiod och sker formellt när bilen är återlämnad till uthyrningskontoret.
                                            </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>

                            <!--[if mso]></td><td style="width:100%;vertical-align:top;" valign="top"><![endif]-->

        				</td>
                	</tr>
            	</tbody></table>
        	</td>
      	</tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td class="padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 50px 0 50px" valign="top">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt">
                            <table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border: none;width: 280px" border="0" cellspacing="0" cellpadding="0" align="left">
                                <tbody><tr>
                                    <td class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;width: 280px">
                                        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td class="brodtextstorrubrik topp" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 26px;line-height: 32px;font-family: Helvetica, Arial, sans-serif;color: #003057;padding: 15px 0 10px 0;text-align: left" valign="top" align="center"><strong>Kom igång med bilen</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="brodtext textpadding" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: left;padding: 0" valign="top" align="center">För din skull har vi samlat allt du behöver veta om din nya bil och hur du aktiverar dina abonnemangstjänster.<br><br><span style="text-decoration: underline;">
                                                    <a class="eGenerator_link" style="color: #003366; text-decoration: underline;" title="Kom igång med din bil" href="http://volvocars.se/v40cbv">LÄS MER</a></span></td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>

                            <!--[if mso]></td><td style="width:100%;vertical-align:top;" valign="top"><![endif]-->

    						<table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border: none;width: 200px" border="0" cellspacing="0" cellpadding="0" align="right">
    							<tbody><tr>
    								<td class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;width: 200px">
    									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
    										<tbody><tr>
    											<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%"><img class="imagepx" style="border: 0;width: 200px" src="https://static.fbinhouse.se/artikel_1_200x200_x2.jpg" alt="Bli frst i Sverige" width="200" border="0"></td>
    										</tr>
    									</tbody></table>
    								</td>
    							</tr>
    						</tbody></table>
        				</td>
                	</tr>
            	</tbody></table>
        	</td>
      	</tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td class="padding-sides-20" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 50px 0 50px" valign="top">
                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt">
                            <table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border: none;width: 280px" border="0" cellspacing="0" cellpadding="0" align="left">
                                <tbody><tr>
                                    <td class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;width: 280px">
                                        <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td class="brodtextstorrubrik topp" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 26px;line-height: 32px;font-family: Helvetica, Arial, sans-serif;color: #003057;padding: 15px 0 10px 0;text-align: left" valign="top" align="center"><strong>Kontakta oss</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="brodtext textpadding" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: left;padding: 00" valign="top" align="center">Frågor om min faktura:<br><a href="mailto:carebyvolvo@bilreda.se">carebyvolvo@bilreda.se</a><br /><br />
                                                    <span style="color: #003366;"><span style="text-decoration: underline;"></span></span></td>
                                            </tr>
                                            <tr>
                                                <td class="brodtext textpadding" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: left;padding: 00" valign="top" align="center">Frågor om min bil: uthyrande kontor<br><br />
                                                    <span style="color: #003366;"><span style="text-decoration: underline;"></span></span></td>
                                            </tr>
                                            <tr>
                                                <td class="brodtext textpadding" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Helvetica, Arial, sans-serif;color: #003057;text-align: left;padding: 00" valign="top" align="center">Övriga frågor: <a href="carebyvolvo@hertz.se">carebyvolvo@hertz.se</a><br><br />
                                                    <span style="color: #003366;"><span style="text-decoration: underline;"></span></span></td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>

                            <!--[if mso]></td><td style="width:100%;vertical-align:top;" valign="top"><![endif]-->

    						<table class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse;border: none;width: 200px" border="0" cellspacing="0" cellpadding="0" align="right">
    							<tbody><tr>
    								<td class="fullWidth" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;width: 200px">
    									<table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
    										<tbody><tr>
    											<td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%"><img class="imagepx" style="border: 0;width: 200px" src="https://static.fbinhouse.se/artikel_2_200x200_x2.jpg" alt="Kontakta oss" width="200" border="0"></td>
    										</tr>
    									</tbody></table>
    								</td>
    							</tr>
    						</tbody></table>
        				</td>
                	</tr>
            	</tbody></table>
        	</td>
      	</tr>
    </tbody></table>




    <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tbody><tr>
            <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 1px;height: 50px;line-height: 0px;font-size: 50px;mso-line-height-rule: exactly" height="50px"><img style="border: 0;display: block;height: 50px" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" height="50" border="0"></td>
        </tr>
    </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>


                                <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#003057">
                                    <tbody><tr>
                                        <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 58px 20px 10px 20px" valign="top">

                                            <table class="brodtextavreg" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody><tr>
                                                    <td class="brodtext2" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Arial, Helvetica, sans-serif;color: #cccccc;text-align: center;padding: 0 0 53px 0;font-weight: bold">
                                                        <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cQy-5pTA/N" target="_blank">
                                                            <span style="color:#cccccc;">Volvocars.se</span>
                                                        </a>
                                                        <span class="mobilebreak"></span>
                                                        <span class="ejmob"><img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="32" height="1" border="0"></span>
                                                        <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/c8t-5pTj/N" target="_blank">
                                                            <span style="color:#cccccc;">Bygg din Volvo</span>
                                                        </a>
                                                        <span class="mobilebreak"></span>
                                                        <span class="ejmob"><img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="32" height="1" border="0"></span>
                                                        <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cpl-5pTM/N" target="_blank">
                                                            <span style="color:#cccccc;">Min Volvo</span>
                                                        </a>
                                                        <span class="mobilebreak"></span>
                                                        <span class="ejmob"><img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="32" height="1" border="0"></span>
                                                        <a data-webversion="true" style="text-decoration:none; color:#cccccc" target="_blank" href="https://rbs.click/cjZ-5pTb/N/?mail=sebastian.borjesson@fbinhouse.se" title="Webbversion">
                                                            <span style="color:#cccccc;">Webbversion</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="brodtext2" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 13px;line-height: 18px;font-family: Arial, Helvetica, sans-serif;color: #cccccc;text-align: center;padding: 0 0 40px 0">

                                                            <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cyS-5pTB/N" target="_blank">
                                                                <span style="color:#cccccc;">Facebook</span>
                                                            </a>

                                                        <img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="17" height="1" border="0">

                                                            <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cZa-5pTF/N" target="_blank">
                                                                <span style="color:#cccccc;">YouTube</span>
                                                            </a>

                                                        <img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="17" height="1" border="0">

                                                            <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/ci8-5pTL/N" target="_blank">
                                                                <span style="color:#cccccc;">Twitter</span>
                                                            </a>

                                                        <span class="mobilebreak"></span>
                                                        <span class="ejmob">
                                                            <img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="17" height="1" border="0">
                                                        </span>

                                                            <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cYT-5pTP/N" target="_blank">
                                                                <span style="color:#cccccc;">Google+</span>
                                                            </a>

                                                        <img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="17" height="1" border="0">

                                                            <a style="text-decoration:none; color:#cccccc" href="https://rbs.click/cCf-5pTo/N" target="_blank">
                                                                <span style="color:#cccccc;">Instagram</span>
                                                            </a>

                                                    </td>
                                                </tr>
                                            </tbody></table>

                                            <table style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tbody><tr>
                                                    <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 0 30px 0" align="center"><img style="border: 0;display: block;text-align: center" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/Images/1.jpg?636735550650950070" alt="Volvo" href="http://www.volvocars.com/se/pages/default.aspx" target="_blank" width="150" border="0"></td>
                                                </tr>
                                            </tbody></table>

                                            <table class="brodtextavreg" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt" width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td class="brodtext2" style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;font-size: 11px;line-height: 22px;font-family: Arial, Helvetica, sans-serif;color: #cccccc;text-align: center;padding: 10px 0 20px 0"><a style="text-decoration:none; color:#cccccc" href="https://rbs.click/crg-5pTD/N" target="_blank">
                                            	<span style="color:#cccccc;text-decoration:underline;">Hantering av personuppgifter</span>
                                            </a>
                                            <span class="mobilebreak"></span>
                                            <span class="ejmob"><img style="border: 0;display: inline" src="https://e2.rbs.click/volvo/_Volvo_Newsletter_v14-Volvo_V60_Cross_Country/OriginalImages/2.gif" width="32" height="1" border="0"></span>
                                            <a style="color:#97999b; text-decoration:none;" href="https://rbs.click/c27-5pTa/N/?yKey=202&email=sebastian.borjesson@fbinhouse.se" title="Avregistrera" target="_blank">
                                            	<span style="color:#cccccc;text-decoration:underline;">Avregistrering</span>
                                            </a></td>
                                        </tr>
                                    </tbody></table>

                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
        </tbody></table>

        <div style="display:none; white-space:nowrap; font:15px courier; color:#F6F6F6;">
            - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        </div>
    </body></html>';

    $userAltMessage = 'Tack för att du väljer Care By Volvo
    En kundmottagare kontaktar dig inom kort för att slutföra bokningen.
    Vid frågor kontakta carebyvolvo@hertz.se';

    try {
        $hertzMailSuccess = false;
        $userMailSuccess = false;

        //HERTZ MAIL
        $hertzMail = new PHPMailer(true);

        //Server settings
        $hertzMail->SMTPDebug = 0;                                 // Enable verbose debug output
        $hertzMail->isSMTP();                                      // Set mailer to use SMTP
        $hertzMail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $hertzMail->SMTPAuth = true;                               // Enable SMTP authentication
        $hertzMail->Username = 'tech@fbinhouse.se';                // SMTP username
        $hertzMail->Password = 'pbypzmbeapglurgf';                 // SMTP password
        $hertzMail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $hertzMail->Port = 587;
        $hertzMail->CharSet = 'utf-8';                             // TCP port to connect to

        //Recipients
        $hertzMail->setFrom( 'no-reply@fbinhouse.se', 'Volvocars.se');
        $hertzMail->addAddress( 'carebyvolvo@hertz.se' );
        $hertzMail->addReplyTo( 'no-reply@fbinhouse.se' );
        $hertzMail->isHTML(true);                                   // Set email format to HTML
        $hertzMail->Subject = $hertzSubject;
        $hertzMail->Body    = $hertzMessage;
        $hertzMail->AltBody = $hertzAltMessage;

        $hertzMailSuccess = $hertzMail->send();

        //USER MAIL
        /*$userMail = new PHPMailer(true);

        //Server settings
        $userMail->SMTPDebug = 0;                                 // Enable verbose debug output
        $userMail->isSMTP();                                      // Set mailer to use SMTP
        $userMail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $userMail->SMTPAuth = true;                               // Enable SMTP authentication
        $userMail->Username = 'tech@fbinhouse.se';                // SMTP username
        $userMail->Password = 'pbypzmbeapglurgf';                 // SMTP password
        $userMail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $userMail->Port = 587;
        $userMail->CharSet = 'utf-8';                             // TCP port to connect to

        //Recipients
        $userMail->setFrom( 'no-reply@fbinhouse.se', 'Volvocars.se');
        $userMail->addAddress( $useremailaddress );
        $userMail->addReplyTo( 'no-reply@fbinhouse.se' );
        $userMail->isHTML(true);                                  // Set email format to HTML
        $userMail->Subject = $userSubject;
        $userMail->Body    = $userMessage;
        $userMail->AltBody = $userAltMessage;

        $userMailSuccess = $userMail->send();*/

        echo ($hertzMailSuccess);
    }
    catch (Exception $e) {
        echo false;
    }
}
?>
