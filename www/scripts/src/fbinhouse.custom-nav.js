( function( $ ){
    fbinhouse.customNav = {
        secondaryNav :
        '<div id="new-secondary-navigation" style="display: block;">' +
            '<div class="new-subnav-bar subnav-dark" data-subnav-hero-url="/se/subnav/{69236EB2-CCB3-4127-8CF8-7197FE92DBEC}">' +
                '<div class="sub-nav-wrap">' +
                    '<div class="pdp-model-wrap">' +
                        '<div class="pdp-model-name">' +
                            '<a class="subnav-toggle subnav-click" data-href="/se/bilar/modeller/xc60">' +
                                'Secondary menu<i class="icon v2nav-icon-angle-down subnav-toggle-icon"></i>' +
                            '</a>' +
                        '</div>' +
                    '<div class="pdp-model-price">&nbspLorem ipsum</div>' +
                '</div>' +
            '</div>' +
        '</div>',

        addSecondaryNav : function(){
            $mainNav = $( '#New-Main-Navigation' );
            $secondaryNav = jQuery.parseHTML( fbinhouse.customNav.secondaryNav );

            if( window.location.href.indexOf( 'localhost' ) > -1 ||
                window.location.href.indexOf( 'hector.fbinhouse.se' ) > -1 ){
                    $mainNav.after( $secondaryNav );
                    $( 'html' ).addClass( 'is-subnav-fixed' );
            }

        }
    }
})( jQuery );
