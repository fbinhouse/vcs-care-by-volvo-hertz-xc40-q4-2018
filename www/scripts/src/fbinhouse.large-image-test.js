// ( function( $, _ ){
//     'use strict';
//     var hundred = 100;
//     var throttleTimeMs = 1000;
//
//     fbinhouse.lrgImg = {
//         resetImgRatio: function( $img, $imgWrapper ){
//             fbinhouse.lrgImg.imgRatio( $img, $imgWrapper );
//             fbinhouse.lrgImg.sizeLargeImage( $img, $imgWrapper );
//         },
//
//         imgRatio: function( $img, $imgWrapper ){
//             var ratio;
//             var defaultRatio = 1.7777777778;
//
//             if( $img.get(0).naturalWidth ) {
//                 ratio = $img.get(0).naturalWidth / $img.get(0).naturalHeight;
//                 console.log( ratio, ( $img.get(0).naturalHeight / $img.get(0).naturalWidth ))
//             } else {
//                 ratio = defaultRatio;
//                 fbinhouse.lrgImg.resetImgRatio( $img );
//             }
//
//             $img.attr( 'ratio', ratio );
//
//             fbinhouse.lrgImg.sizeLargeImage( $img, $imgWrapper );
//         },
//
//         containPad: function( $img ){
//             var pad = ( $img.get(0).naturalHeight / $img.get(0).naturalWidth ) * 100;
//
//             return pad + '%';
//         },
//
//         sizeLargeImage: function( $img, $imgWrapper ){
//             var w = $imgWrapper.outerWidth();
//             var h = $imgWrapper.outerHeight();
//             var ratio = w / h;
//             var imgRatio = $img.attr( 'ratio' );
//             var pos = $img.attr( 'data-alignment' );
//             var m = {
//                 'center': 1,
//                 'left': 0,
//                 'right': 1.95,
//                 'bottom': 2,
//                 'top': 0
//             };
//             var style;
//             var y;
//             var x;
//             var align = {
//                 x: 1,
//                 y: 2
//             };
//
//     				if( pos ){
//                 pos = $img.attr( 'data-alignment' ).split( ' ' );
//
//                 if( pos[0] ){
//                     if( pos[0] === 'center' || pos[0] === 'left' || pos[0] === 'right' ){
//                         align.x = m[ pos[0] ];
//                     } else {
//                         align.x = parseInt( pos[0], 10 ) / hundred * 2;
//                     }
//                 }
//                 if( pos[1] ){
//                     if( pos[1] === 'center' || pos[1] === 'top' || pos[1] === 'bottom' ){
//                         align.y = m[ pos[1] ];
//                     } else {
//                         align.y = parseInt( pos[1], 10 ) / hundred * 2;
//                     }
//                 }
//             }
//
//
//             if( ratio > imgRatio ){
//                 y = Math.ceil( w / imgRatio - h );
//                 x = -1;
//             } else {
//                 y = 0;
//                 x = Math.ceil((( h + y ) * imgRatio - w ) / 2 * align.x );
//             }
//
//             style = {
//                 width: Math.ceil(( h + y ) * imgRatio ),
//                 height: Math.ceil( h + y ),
//                 top: -Math.abs( y / 2 * align.y ),
//                 left: -Math.abs( x )
//             };
//
//             $img.css( style );
//         },
//
//         largeImage: function( selector ){
//             var $img = $( selector );
//             var contain = $img[0].hasAttribute( 'data-contain' ) ? true : false;
//             var $imgWrapper;
//             var src = $img.attr( 'data-src' );
//             var split = src.split( '.' ).length - 2;
//             var name = src.split( '.' )[ split ].split( '/' ).pop();
//             var path = src.substring( 0, src.indexOf( name ));
//             var srcset = path + '480/' + name + '.jpg 480w, ' + path + '768/' + name + '.jpg 768w, ' + path + '1024/' + name + '.jpg 1024w, ' + path + '2048/' + name + '.jpg 2048w, ' + path + '4096/' + name + '.jpg 4096w';
//             var sizeImg;
//             var desktopSource = $( '<source>', {
//                 'media': '( min-width:3em )',
//                 'srcset': srcset,
//                 'sizes': '100vw'
//             });
//             var fallback = $( '<img>', { 'src': path + '768/' + name + '.jpg' });
//             var attributes = {
//                 'srcset': srcset,
//                 'sizes': '100vw'
//             };
//
//             $img.wrap( '<div class="large-image-wrapper"></div>' );
//             $imgWrapper = $img.parent();
//
//             if( contain ){
//                 $imgWrapper
//                     .addClass( 'large-image-contain' )
//                     .css( 'padding-top', fbinhouse.lrgImg.containPad( $img ));
//             }
//
//             $img.append([
//                 desktopSource,
//                 fallback
//             ]);
//
//     		sizeImg = function(){
//                 if( $imgWrapper.height() < hundred * ( 2 + 1 )){
//                     setTimeout( function(){
//                         sizeImg();
//                     }, hundred );
//                 } else {
//                     fbinhouse.lrgImg.imgRatio( $img, $imgWrapper );
//                 }
//             };
//
//             $img.on( 'load', function(){
//                 sizeImg();
//             });
//         },
//
//         initLargeImages: function(){
//             fbinhouse.lrgImg.largeImages = [];
//             console.log( 'test' );
//             $( 'picture' ).each( function(){
//                 if( $( this ).attr( 'data-src' ) !== undefined && $( this ).attr( 'data-src' ).indexOf( 'srcsets' ) !== -1 && !$( this ).parent().is( '.gallery-item' )){
//                     console.log( $( this ));
//                     if( !$( this )[0].hasAttribute( 'data-contain' ))
//                         fbinhouse.lrgImg.largeImages.push( this );
//
//                     if( !$( this ).parent().is( '.large-image-wrapper' ))
//                         fbinhouse.lrgImg.largeImage( this );
//                 }
//             });
//         },
//
//     	resizeLargeImages: function(){
//             $.each( fbinhouse.lrgImg.largeImages, function(){
//                 fbinhouse.lrgImg.sizeLargeImage( $( this ), $( this ).parent());
//             });
//         }
//
//     }
//
//     $( window ).resize( function(){
//         _.throttle( fbinhouse.lrgImg.resizeLargeImages(), throttleTimeMs );
//     });
//
//     fbinhouse.initLargeImages = fbinhouse.lrgImg.initLargeImages;
//     fbinhouse.resizeLargeImages = fbinhouse.lrgImg.resizeLargeImages;
// })( jQuery, _ );
