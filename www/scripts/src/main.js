fbinhouse.init = function( $ ){
    'use strict';

    //Init pixels
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var optoutmultiCookie = getCookie('OPTOUTMULTI');
    var optoutmultiSplit = optoutmultiCookie.split('|');
    var targetCookie;

    fbinhouse.detectIe( $ );

    $( 'html' ).addClass( 'loaded' );

    fbinhouse.initLargeImages();
    fbinhouse.callbacks.initCallbacks();

    $( '.fbi-headerblock' ).imagesLoaded()
        .done( function(){
            fbinhouse.preloader.fadeOut();
        });

        $( '#fbi-content' ).imagesLoaded()
        .done( function( elem ){

            fbinhouse.resizeLargeImages();
            $('.carousel').flickity({
               pageDots: true,
               contain: true,
               imagesLoaded: true
            })

            window.onscroll = _.throttle( mySticyBarfunction, 200);

            // google analytic
            window.setTimeout(function() {
                if ("ga" in window) {
                    var tracker = ga.getAll()[1];
                    if (tracker) {
                        tracker.set('page', '/se/kop/finansiering-och-tjanster/privat/care-by-volvo/flex/startad/xc40');
                        tracker.send('pageview');
                    }
                }
            }, 4000);
        });


        // sticky bar
        //$(window).scroll( _.throttle( mySticyBarfunction, 100 ));
        var navBar = $('.pris-next-btn');
        var stickbar = navBar.offset().top;
        var $endForm = $('.carousel').offset().top + $('.carousel').height() ;
        var $startScroll = (window.innerHeight / 2) - $('.hertzContent').height(); // $('.text-align-right').offset().top + 100;
        // $('.pris-next-btn').addClass('startPos');


        function mySticyBarfunction() {
            // console.log('Y: ', window.pageYOffset, 'head: ', $startScroll )
            if (window.pageYOffset >= $startScroll && window.pageYOffset <= $endForm) {

                if(!navBar.hasClass('stickBar')){
                    navBar.addClass('stickBar').hide().slideDown('slow');
                }
                // console.log('addClass');
            } else {
                navBar.removeClass('stickBar');
                // console.log('removeClass');
            }
        }



        // // Engine select
        // $('.hertzChoiceForm .engineSelect .formButton').on('click', function(e) {
        //     e.preventDefault();
        //
        //     if($(this).hasClass('selected')) {
        //         $(this).removeClass('selected');
        //     } else {
        //         $('.hertzChoiceForm .engineSelect .formButton').removeClass('selected');
        //         $(this).addClass('selected');
        //     }
        //
        //     var engine = $(this).data('value').toLowerCase() == 'bensin' ? 'T5' : 'D4';
        //     $('.step2 .selectedXC40').text('Volvo XC40 ' + engine + ' AWD, aut');
        // });

        // City select
        $('.hertzChoiceForm .citySelect .formButton').on('click', function(e) {
            e.preventDefault();

            if($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('.hertzChoiceForm .citySelect .formButton').removeClass('selected');
                $(this).addClass('selected');
            }

            if ("ga" in window) {
                var tracker = ga.getAll()[1];
                if (tracker) {
                    tracker.set('page', '/se/kop/finansiering-och-tjanster/privat/care-by-volvo/flex/steg1/xc40');
                    tracker.send('pageview');
                }
            }
        });

        // tyreSelect select
        $('.hertzChoiceForm .tyreSelect .formButton').on('click', function(e) {
            e.preventDefault();

            if($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('.hertzChoiceForm .tyreSelect .formButton').removeClass('selected');
                $(this).addClass('selected');
            }
        });

        $('.carInfo .closeIcon').on('click', function() {
            $('.carInfo').hide();
        });


        // Step 1 > 2
        $('.step1 .orderBtn').on('click', function(e) {
            e.preventDefault()
            if($('.formButton').hasClass('selected') && $('#dateSelect1').val()) {

                //google analytic

                if ("ga" in window) {
                    var tracker = ga.getAll()[1];
                    if (tracker) {
                        tracker.set('page', '/se/kop/finansiering-och-tjanster/privat/care-by-volvo/flex/steg2/xc40');
                        tracker.send('pageview');
                    }
                }


                $('.stepMenu .step').removeClass('currentStep');
                $('.stepMenu #step2').addClass('currentStep');

                $('.step1').css('display', 'none');
                $('.step2').css('display', 'block');
                $('.step3').css('display', 'none');
                $('.step1Failed').hide();

                $('html, body').animate({
                    scrollTop: $("#contactForm").offset().top - 100
                }, 200);

                // Start
                $.each(optoutmultiSplit, function() {
                    if($(this).selector.indexOf('c6') == 0) {
                        targetCookie = $(this).selector;
                    };

                    if(String(targetCookie).slice(-1) == '0') {
                        $('body').prepend('<img src="https://secure.adnxs.com/px?id=1034507&seg=14681584&t=2" width="1" height="1" />');
                        $('body').prepend('<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1789914651042181&ev=PageView&noscript=1" />');
                        $('body').prepend('<img src="https://ad.doubleclick.net/ddm/activity/src=8337914;type=orders;cat=cbvflexs;u1=[Nameplate];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=[SessionID]?" width="1" height="1" alt=""/>');
                        $('body').prepend('<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1789914651042181&ev=FlexOrderStart&noscript=1" />');
                    }
                });

            } else {
                $('.step1Failed').show();
                $('.step1Failed')[0].scrollIntoView(false);
                showUnselected($('.formButton'));
                dateIsSelected($('#dateSelect1'));
            }

            function showUnselected(obj){
                if(obj.hasClass('selected')){
                    obj.removeClass('highlight');
                } else {
                    obj.addClass('highlight');
                }
            }
            function dateIsSelected(obj){
                if(obj.val()){
                    obj.removeClass('highlight');
                } else {
                    obj.addClass('highlight');
                }
            }

            $('#contactForm #carCity').attr('value', $('.hertzChoiceForm .citySelect .selected').data('value'));
            $('#contactForm #color').attr('value', $('.hertzChoiceForm .color.selected').data('color'));
            $('#contactForm #startDate').attr('value', $('.hertzChoiceForm #dateSelect1').val());
            $('#contactForm #length').attr('value', $('.lengthText p span').text());
        })

        // info cars popups
        $('.hertzBlock .eq-info').on('click', function(e) {
            e.preventDefault();
            $('.info-lightBox').fadeIn();
            $('.carInfo').show();
            $('html').addClass('noScroll');
        });

        $('.hertzBlock .closeIcon').on('click', function() {
            $('.carInfo').hide();
            $('.info-lightBox').fadeOut();
            $('html').removeClass('noScroll');
        });

        $(document).mouseup(function(e) {
            var container = $('.carInfo');

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                container.hide();
                $('.info-lightBox').fadeOut();
                $('html').removeClass('noScroll');
            }
        });
        // ** Step 2 ** //

        // Back to 1
        $('.step2 .backBtn, .stepMenu #step1').on('click', function(e) {
            e.preventDefault()
            $('html, body').animate({
                scrollTop: $(".input-name").offset().top - 100
            }, 200);
            $('.hertzChoiceForm .formSelect').removeClass('selected');
            $('.formSelect .formButton').removeClass('selected');

            $('.stepMenu .step').removeClass('currentStep');
            $('.stepMenu #step1').addClass('currentStep');

            $('.step1').css('display', 'block');
            $('.step2').css('display', 'none');
            $('.step3').css('display', 'none');

        })

        $("#contactForm .orderBtn").on('click', function(e) {
            $('.stepFailed').hide();

            e.preventDefault();

            var formValidated = true;

            $('input:text').filter('[required]').each(function(index, value){
                if ($(value).val().length < 1) {
                    formValidated = false;
                    return false;
                }
            });

            if (!$('#contactForm input:checkbox').is(':checked')) {
                formValidated = false;
            }

            if (!formValidated) {
                $('.step2Failed.validationFailed').show();
                $('.step2Failed')[0].scrollIntoView(false);
            }
            else {
                $('.stepFailed').hide();
                //google analytic

                if ("ga" in window) {
                    var tracker = ga.getAll()[1];
                    if (tracker) {
                        tracker.set('page', '/se/kop/finansiering-och-tjanster/privat/care-by-volvo/flex/steg3/xc40');
                        tracker.send('pageview');
                    }
                }
                $.ajax({
                    type: "POST",
                    url: $('#contactForm').attr('action'),
                    data: $("#contactForm").serialize(),
                    success: function(data)
                    {
                        if (data == '1') {
                            $('.stepMenu .step').removeClass('currentStep');
                            $('.stepMenu #step3').addClass('currentStep');

                            $('.step1').css('display', 'none');
                            $('.step2').css('display', 'none');
                            $('.step3').css('display', 'block');
                            $('.step2Failed').hide();
                            $('html, body').animate({
                                scrollTop: $(".step3").offset().top - 100
                            }, 200);

                            //google analytic
                            if ("ga" in window) {
                                var tracker = ga.getAll()[1];
                                if (tracker) {
                                    tracker.set('page', '/se/kop/finansiering-och-tjanster/privat/care-by-volvo/flex/avsluta/xc40');
                                    tracker.send('pageview');
                                }
                            }
                        } else {
                            $('.step2Failed.formPostFailed').show();
                            $('.step2Failed')[0].scrollIntoView(false);
                        }
                    }
                });

                // Finish
                $.each(optoutmultiSplit, function() {
                    if($(this).selector.indexOf('c6') == 0) {
                        targetCookie = $(this).selector;
                    };

                    if(String(targetCookie).slice(-1) == '0') {
                        $('body').prepend('<img src="https://secure.adnxs.com/px?id=1034508&seg=14681591&t=2" width="1" height="1" />');
                        $('body').prepend('<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1789914651042181&ev=PageView&noscript=1" />');
                        $('body').prepend('<img src="https://ad.doubleclick.net/ddm/activity/src=8337914;type=orders;cat=cbvflexf;u1=[Nameplate];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=[SessionID]?" width="1" height="1" alt=""/>');
                        $('body').prepend('<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1789914651042181&ev=FlexOrderFinished&noscript=1" />');
                    }
                });
            }
        });


    /* ----- SLIDER ----- */

    var hostImg = 'https://static.fbinhouse.se/';
    var imgsize = '1200x/';
    var interior = '_INTERIOR_';
    var exterior = '_EXTERIOR_';
    var xc40ccSliderCode = '1953613120C11071100R100000000000000MC0B60MC0B90';
    var mockUpCarousel = '';

    function buildCarousel(cisCode){
        mockUpCarousel = '<div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_1.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_2.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_3.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_4.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_5.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_EXTERIOR_6.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_INTERIOR_1.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_INTERIOR_2.png"/></div><div class="carousel-cell"><img src="' +
        hostImg +
        imgsize +
        cisCode + '_INTERIOR_3.png"/></div>';

        return mockUpCarousel;
    }

    $('.carousel').append(buildCarousel(xc40ccSliderCode));



    var minDate = new Date().fp_incr(+10);
    var maxDate = new Date('2018-12-17');

    //$('.permittedDates').text('(Måste vara ett datum mellan ' + minDate.getDate() + '/' + (minDate.getMonth() + 1) + ' och ' + maxDate.getDate() + '/' + (maxDate.getMonth() + 1) + ')');

    $('#dateSelect1').pickadate({
        monthsFull:["januari","februari","mars","april","maj","juni","juli","augusti","september","oktober","november","december"],
        monthsShort:["jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"],
        weekdaysFull:["söndag","måndag","tisdag","onsdag","torsdag","fredag","lördag"],
        weekdaysShort:["sön","mån","tis","ons","tor","fre","lör"],
        firstDay: 1,
        format: "yyyy-mm-dd",
        today: "Idag",
        formatSubmit: "yyyy/mm/dd",
        labelMonthNext: "Nästa månad",
        labelMonthPrev: "Föregående månad",
        labelMonthSelect: "Välj månad",
        labelYearSelect: "Välj år",
        close: 'Stäng',
        min: minDate,
        max: maxDate,
        onClose: function() {
            $('#dateSelect1').blur();
            $(document.activeElement).blur();
            $('html').removeClass('removeScroll');
        },
        onOpen: function() {
            $('html').addClass('removeScroll');
            console.log('opened');
        }
    });

    $('.question').on('click', function(e) {
        e.preventDefault();
        var target = $(this);
        if( !$(this).next().hasClass('hidden') ) {
            target.next().slideDown(200);
            target.next().addClass('hidden');
            target.addClass('hidden');
        } else {
            target.next().slideUp(200);
            target.next().removeClass('hidden');
            target.removeClass('hidden');
        }
    });

};
