<?php
    $spreadsheetUrl="https://docs.google.com/spreadsheets/d/e/2PACX-1vSG4X1hTYKWJd_VaIbJ0CVVt2TWmda_qSfOgdNMGGd5y3MolHK9STdetLAbkCcWq8BndEWh2NElnMOa/pub?gid=0&single=true&output=csv";
    $csv= file_get_contents($spreadsheetUrl);
    $csvArray = array_map("str_getcsv", explode("\n", $csv));
    $cars = array();

    foreach($csvArray as $key=>$row) {
        if ($key != 0) {
            $car = (object) [
                'cisCode' => $row[0],
                'color' => $row[1],
                'available' => $row[2]
            ];
        
            array_push($cars, $car);
        }
    }
    
    $json = (object)[
        'cars' => $cars
    ];

    // $jsonFile = 'availableCarsCallback('.json_encode($json).')';
    $jsonFile = json_encode($json);

    print_r($jsonFile);

    $fp = fopen('generated/cbv-hertz-available-cars.json', 'w');
    fwrite($fp, $jsonFile);
    fclose($fp);
?>