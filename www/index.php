<?php
    error_reporting( E_ALL & ~E_WARNING & ~E_NOTICE );

    $templateUrl = 'http://devil.fbinhouse.se/inhouse-volvocars-page-mirror/';
    $targetMarkup = '<footer';

    $fullPage = file_get_contents( $templateUrl );

    $fullPage = str_ireplace(
        $targetMarkup,
        '<script src="scripts/fbinhouse-injector.dev.js"></script>' . "\n" . $targetMarkup,
        $fullPage
    );

    echo $fullPage;
