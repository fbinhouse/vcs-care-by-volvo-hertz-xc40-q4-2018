var request = require( 'request' );

module.exports = function( grunt ){
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON( 'package.json' ),
        watch: {
            options: {
                livereload: true
            },
            less: {
                files: [ 'www/less/**/*.less' ],
                tasks: [ 'less', 'postcss', 'cssmin:default'/*, 'lesshint'*/ ]
            },
            scripts: {
                files: [ 'www/scripts/**/*.js', '!www/scripts/injector.js', '!www/scripts/fbinhouse-injector.dev.js', '!www/scripts/fbinhouse-injector.js', '!www/scripts/merge.dev.js', '!www/scripts/merge.live.js', '!www/scripts/merge.live.min.js', '!www/scripts/merge.dev.min.js', '!www/scripts/dependencies.js', '!www/scripts/live.cors.min.js', '!www/scripts/dev.cors.min.js' ],
                tasks: [ /*'eslint:default', 'eslint:defaultJSON',*/ 'concat:build_dev', 'concat:build_live' ]
            },
            depscripts: {
                files: [ 'build/dependencies.js', 'build/live.cors.min.js' ],
                tasks: [ 'concat:build_dev', 'concat:build_live' ]
            },
            build_all: {
                files: [ 'build/merge.dev.min.js' ],
                tasks: [ 'concat:buildtest_dev', 'concat:buildtest_live' ]
            },
            loader: {
                files: [ 'www/scripts/src/loader.js' ],
                tasks: [ 'eslint:loader', 'eslint:loaderJSON' ]
            },
            gruntfile: {
                files: [ 'Gruntfile.js' ],
                tasks: [ /*'eslint:gruntfile', 'eslint:gruntfileJSON'*/ ]
            },
            buildScripts: {
                files: [ '*.js', '!Gruntfile.js' ],
                tasks: [ 'eslint:build', 'eslint:buildJSON' ]
            },
            injector: {
                files: [ 'www/scripts/injector.js' ],
                tasks: [ 'eslint:injector', 'eslint:injectorJSON' ]
            },
            build: {
                files: [ 'build/merge.dev.js' ],
                tasks: [ 'uglify:build_dev', 'uglify:build_live' ]
            },
            responsive_images: {
                files: [ 'www/img/srcsets/**.{jpg,png}' ],
                tasks: [ 'responsive_images:dev' ]
            },
            download_dependencies: {
                files: [ 'package.json' ],
                tasks: [ 'execute:download_dependencies' ]
            },
            inlinecss: {
                files: [ 'www/content.html', 'build/style.min.css' ],
                tasks: [ 'string-replace:css' ]
            },
            mergeAll: {
                files: [ 'build/content.inline.html' ],
                tasks: [ 'replacer:dev', 'replacer:live', 'minifyHtml:dist', 'string-replace:default'/*, 'comboall:dev', 'comboall:live'*/ ]
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require( 'autoprefixer' )(),
                    require( 'postcss-opacity' )()
                ]
            },
            default: {
                src: 'build/style.css',
                dest: 'build/style.prefix.css'
            }
        },
        cssmin: {
            default: {
                options: {
                    compatibility: 'ie8',
                    advanced: false, // Needed for property override
                    sourceMap: true,
                    rebase: false
                },
                src: [ 'build/style.prefix.css' ],
                dest: 'build/style.min.css'
            }
        },
        less: {
            default: {
                options: {
                    paths: [ 'www/less' ],
                    sourceMap: true,
                    sourceMapFilename: 'build/style.css.map',
                    sourceMapURL: 'style.css.map',
                    sourceMapBasepath: 'www',
                    sourceMapRootpath: '../'
                },
                src: 'www/less/style.less',
                dest: 'build/style.css'
            }
        },
        lesshint: {
            default: {
                options: {
                    force: true,
                    lesshintrc: true,
                    strictImports: false
                },
                files: {
                    src: [ 'www/less/**/*.less', '!www/less/overrides.less' ]
                }
            },
            overrides: {
                options: {
                    force: true,
                    importantRule: false,
                    idSelector: false
                },
                files: {
                    src: [ 'www/less/overrides.less' ]
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            build_dev: {
                src: [ 'build/dependencies.js', 'www/scripts/injector.js', 'build/dev.cors.min.js', 'www/scripts/libs/*.js', 'www/scripts/polyfill/*.js', 'www/scripts/src/*.js', '!www/scripts/src/loader.js', 'www/scripts/src/loader.js' ],
                //src: [ 'www/scripts/src/_redirect.js' ],
                dest: 'build/merge.dev.js'
            },
            build_live: {
                /*src: [ 'build/dependencies.js', 'www/scripts/injector.js', 'build/live.cors.min.js', 'www/scripts/libs/*.js', 'www/scripts/polyfill/*.js', 'www/scripts/src/*.js', '!www/scripts/src/loader.js', 'www/scripts/src/loader.js' ],*/
                src: [ 'www/scripts/src/_redirect.js' ],
                dest: 'build/merge.live.js'
            },
            buildtest_dev: {
                src: [ 'build/merge.dev.min.js' ],
                dest: 'www/scripts/fbinhouse-injector.dev.js'
            },
            buildtest_live: {
                src: [ 'build/merge.live.min.js' ],
                dest: 'www/scripts/fbinhouse-injector.js'
            }
        },
        eslint: {
            options: {
                configFile: 'config/eslint-default.json'
            },
            default: {
                files: {
                    src: [ 'www/scripts/**/*.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/libs/**/*.js', '!www/scripts/injector.js', '!www/scripts/src/loader.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js', '!www/scripts/fbinhouse-injector.test.js' ]
                }
            },
            defaultJSON: {
                options: {
                    format: 'json',
                    outputFile: 'results/default.json'
                },
                files: {
                    src: [ 'www/scripts/**/*.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/libs/**/*.js', '!www/scripts/injector.js', '!www/scripts/src/loader.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js', '!www/scripts/fbinhouse-injector.test.js' ]
                }
            },
            gruntfile: {
                options: {
                    configFile: 'config/eslint-grunt.json'
                },
                files: {
                    src: [ 'Gruntfile.js' ]
                }
            },
            gruntfileJSON: {
                options: {
                    configFile: 'config/eslint-grunt.json',
                    format: 'json',
                    outputFile: 'results/grunt.json'
                },
                files: {
                    src: [ 'Gruntfile.js' ]
                }
            },
            injector: {
                files: {
                    src: [ 'www/scripts/injector.js' ]
                }
            },
            injectorJSON: {
                options: {
                    format: 'json',
                    outputFile: 'results/injector.json'
                },
                files: {
                    src: [ 'www/scripts/injector.js' ]
                }
            },
            loader: {
                options: {
                    configFile: 'config/eslint-loader.json'
                },
                files: {
                    src: [ 'www/scripts/src/loader.js' ]
                }
            },
            loaderJSON: {
                options: {
                    configFile: 'config/eslint-loader.json',
                    format: 'json',
                    outputFile: 'results/loader.json'
                },
                files: {
                    src: [ 'www/scripts/src/loader.js' ]
                }
            },
            build: {
                options: {
                    configFile: 'config/eslint-build.json',
                    fix: true
                },
                files: {
                    src: [ '*.js', '!Gruntfile.js' ]
                }
            },
            buildJSON: {
                options: {
                    configFile: '',
                    format: 'json',
                    fix: true,
                    outputFile: 'results/build.json'
                },
                files: {
                    src: [ '*.js', '!Gruntfile.js' ]
                }
            }
        },
        jscs: {
            options: {
                config: '.jscsrc',
                force: true
            },
            default: [ 'www/scripts/**/*.js', '!www/scripts/libs/**/*.js', '!www/scripts/polyfill/**/*.js', '!www/scripts/merge.js', '!www/scripts/merge.min.js' ]
        },
        uglify: {
            options: {
                compress: {},
                sourceMap: true
            },
            build_dev: {
                src: [ 'build/merge.dev.js' ],
                dest: 'build/merge.dev.min.js'
            },
            build_live: {
                src: [ 'build/merge.live.js' ],
                dest: 'build/merge.live.min.js'
            }
        },
        responsive_images: {
            dev: {
                options: {
                    sizes: [{
                        width: 240,
                        quality: 100
                    },
                    {
                        width: 480,
                        quality: 80
                    },
                    {
                        width: 768,
                        quality: 80
                    },
                    {
                        width: 1024,
                        quality: 80
                    },
                    {
                        width: 2048,
                        quality: 60
                    },
                    {
                        width: 4096,
                        quality: 60
                    }]
                },
                files: [{
                    expand: true,
                    src: [ 'img/srcsets/**.{jpg,png}' ],
                    cwd: 'www/',
                    custom_dest: 'www/img/srcsets/{%= width %}'
                }]
            }
        },
        execute: {
            download_dependencies: {
                call: function( gruntObj, options, async ){

                    var pkg = gruntObj.file.readJSON( 'package.json' );

                    var done = async();
                    var okStatus = 200;

                    if( pkg.fbinhouse && pkg.fbinhouse.scriptDependencies ){
                        gruntObj.log.writeln( 'Read package.json: ' + pkg.fbinhouse.scriptDependencies.join( ',' ));
                        request( 'http://cdn.jsdelivr.net/g/' + pkg.fbinhouse.scriptDependencies.join( ',' ), function( error, response, body ){
                            if( !error && response.statusCode === okStatus ){
                                grunt.file.write( 'build/dependencies.js', body );
                                done();
                            } else {
                                done( error );
                            }
                        });
                    } else {
                        gruntObj.log.writeln( 'Read package.json: No dependencies found' );
                    }
                }
            }
        },
        replacer: {
            dev: {
                options : {
                    replace: {
                        '@@env': 'dev',
                        '@@path' : function(){
                            return '.';
                        }
                    }
                },
                files : [
                    {
                        src: [ 'build/content.inline.html' ],
                        dest: 'build/dev.html'
                    }
                ]
            },
            live: {
                options : {
                    replace: {
                        '@@path' : function(){
                            var pkg = grunt.file.readJSON( 'package.json' );
                            return 'https://' + pkg.fbinhouse.deploy.host + '/' + pkg.name;
                        },
                        '@@env': 'live'
                    }
                },
                files : [
                    {
                        src: [ 'build/content.inline.html' ],
                        dest: 'build/live.html'
                    }
                ]
            }
        },
        inline: {
            dev: {
                src: 'www/content.html',
                dest: 'build/content.dev.html'
            },
            live: {
                src: 'www/content.html',
                dest: 'build/content.live.html'
            }
        },
        minifyHtml: {
            dist: {
                files: {
                    'build/dev.min.html': 'build/dev.html',
                    'build/live.min.html': 'build/live.html'
                }
            }
        },
        'string-replace': {
            default: {
                files: {
                    'build/live.cors.min.js': 'build/live.min.html',
                    'build/dev.cors.min.js': 'build/dev.min.html'
                },
                options: {
                    replacements: [
                        {
                            pattern: /^[\s\S]*$/gi,
                            replacement: function( match ){
                                return '/* globals fbinhouse */fbinhouse.pageContent = ' + JSON.stringify( match ) + ';';
                            }
                        }
                    ]
                }
            },
            css: {
                files: {
                    'build/content.inline.html': 'www/content.html'
                },
                options: {
                    replacements: [
                        {
                            pattern: /^[\s\S]*$/gi,
                            replacement: function( match ){
                                var css = grunt.file.read( 'build/style.min.css' );
                                return '<style>' + css + '</style>' + match;
                            }
                        }
                    ]
                }
            }
        }
    });

    require( 'load-grunt-tasks' )( grunt );

    grunt.registerTask( 'default', [] );
};
