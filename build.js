var fs = require( 'fs' );
var colors = require( 'colors' );
var filename = 'www/scripts/fbinhouse-injector.js';

function getProjectName(){
    'use strict';
    return __dirname.split( '/' ).pop();
}

function getProjectRepositoryUrl(){
    'use strict';
    var repositoryUrl;
    var fileData;

    if( fs.existsSync( '.git/config' )){
        fileData = fs.readFileSync( '.git/config', 'utf8' );
        repositoryUrl = fileData.match( /url\s\=(.+?)\.git/gim )[0];

        repositoryUrl = repositoryUrl.replace( 'url = ', '' );
        repositoryUrl = repositoryUrl.replace( '.git', '' );
        repositoryUrl = repositoryUrl.replace( /\w+@/, '' );
    } else {
        console.log( colors.yellow( 'Did not find .git/config, can\'t set repository' ));
    }

    return repositoryUrl;
}

function writeFile( contents ){
    'use strict';
    fs.writeFile( filename, contents, function( error ) {
        if( error ) {
            console.log( error.red );
        } else {
            console.log( 'Project name set to "' + getProjectName() + '" in fbinhouse-injector.js' );
        }
    });
}

function processFile( contents ){
    'use strict';
    var parsedContents = '';

    if( contents.indexOf( 'vcs-sitecore-template' ) === -1 ){
        console.log( colors.red( 'Unable to find "vcs-sitecore-template" in ' + filename + '. Project name NOT set.' ));
        return false;
    }

    parsedContents = contents.replace( 'vcs-sitecore-template', getProjectName());

    writeFile( parsedContents );

    return true;
}

function setPackageVar( name, value ){
    'use strict';
    var projectSettings;
    var num = 4;
    var packageData = fs.readFileSync( 'package.json', {
        encoding: 'utf8'
    });

    if( fs.existsSync( 'package.json' )){
        projectSettings = JSON.parse( packageData );
        projectSettings[ name ] = value;

        fs.writeFileSync( 'package.json', JSON.stringify( projectSettings, null, num ));
        console.log( 'Set ' + name + ' in package.json to to "' + value + '"' );

        return true;
    }

    console.log( colors.inverse( 'Couldn\'t find package.json. Skipping project ' + name + ' update.' ));
    return false;
}

fs.exists( filename, function( exists ) {
    'use strict';
    if ( exists ) {
        fs.readFile( filename, 'utf8', function( error, data ){
            processFile( data );

            return true;
        });
    } else {
        console.log( colors.inverse( 'Couldn\'t find file ' + filename + '. Skipping project name setup' ));
    }
});

fs.exists( 'package.json', function( exists ){
    'use strict';
    if( exists ){
        setPackageVar( 'name', getProjectName());
        setPackageVar( 'repository', getProjectRepositoryUrl());
    }
});
